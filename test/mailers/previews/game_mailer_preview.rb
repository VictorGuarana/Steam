# Preview all emails at http://localhost:3000/rails/mailers/game_mailer
class GameMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/game_mailer/new_game
  def new_game
    GameMailer.new_game
  end

end
