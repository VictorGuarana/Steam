class CreateGameCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :game_categories do |t|
      t.string :name
      t.references :Game, foreign_key: true
      t.references :Category, foreign_key: true

      t.timestamps
    end
  end
end
