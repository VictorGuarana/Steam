# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Game.destroy_all #limpar oq tem antes

#e = Game.create(name: "Skyrin VI", description: "Um dia chega", publication: Time.now)

50.times do |i|
    Game.create(name: "GTA " + i.to_s,
        description: "Um dia chega",
        price:rand(99.99 .. 200.99), 
        publication: Time.now);
end

#50.times do |i|
#    User.create(name: "Guarana #"+i.to_s, 
#        email: "email"+i.to_s+"@email.com");
#end

r = Region.create(name: "Brasil");
