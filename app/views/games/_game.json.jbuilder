json.extract! game, :id, :name, :description, :picture, :price, :publication, :created_at, :updated_at
json.url game_url(game, format: :json)
