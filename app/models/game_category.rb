class GameCategory < ApplicationRecord
  belongs_to :Game
  belongs_to :Category
end
