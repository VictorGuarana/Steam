class GameMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.game_mailer.new_game.subject
  #
  def new_game(game)
    @greeting = "Koe"
    @users = User.first
    @game = game

    mail to: @users.email, subject: "Novo Jogo"
  end
end
